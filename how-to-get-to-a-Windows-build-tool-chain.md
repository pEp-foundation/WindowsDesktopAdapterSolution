# Getting to a Windows build tool chain

- use Windows 10 or Windows 11; this was tested using Windows 11 Professional
- install Visual Studio 2022 from https://visualstudio.microsoft.com/vs/community/ with Windows Desktop Development C++ and Python 64bit
- install Rust from https://www.rust-lang.org/tools/install using 64-bit, default options
- install MSYS2 from https://www.msys2.org/#installation using 64bit, default options
- add `C:\MSys64\usr\bin` to your PATH user environment variable
- start x64 Native Tools Command Prompt for VS 2022 in the Start menu
- on the command line enter this to install Rust 1.60.0:

```
C:\Program Files\Microsoft Visual Studio\2022\Community> rustup default "1.60.0"
```

- Start the MSYS2 CLANG64 command line; on this shell install git:

```
$ pacman -S git
```

- add the installed Python 64bit coming from Visual Studio 2022 to the PATH by adding `C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python39_64` to your user environment variables (or the path where you installed it into)
- move this path to the top/beginning of the PATH variable, or it will not work
- go to the x64 Native Tools Command Prompt for VS 2022 and try if it can find Python; you may need to restart this program before it finds Visual Studio's Python

```
C:\Program Files\Microsoft Visual Studio\2022\Community> python
Python 3.9.7 (tags/v3.9.7:1016ef3, Aug 30 2021, 20:19:38) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> quit()
```

- upgrade pip and install lxml

```
C:\Program Files\Microsoft Visual Studio\2022\Community> py -m pip install --upgrade pip
C:\Program Files\Microsoft Visual Studio\2022\Community> py -m pip install lxml
```

- disable compression in git; there is a bug on Windows, which can be circumvented like this

```
C:\Program Files\Microsoft Visual Studio\2022\Community> git config --global core.compression 0
```

- go to the repos directory and clone the YML2 repo

```
C:\Program Files\Microsoft Visual Studio\2022\Community> cd %USERPROFILE%\source\repos
C:\Users\vb\source\repos> git clone ssh://newgitea@gitea.pep.foundation:23065/fdik/yml2.git
```

- set the `YML_HOME` user environment variable to `%USERPROFILE%\source\repos\yml2` (only one `...\yml2` at the end) and restart the x64 Native Tools Command Prompt for VS 2022
- in case you have formerly set `YML_PATH` then unset this environment variable
- test the `yml2proc` tool; enter the first two lines of the hello world programm after the second command, then press CTRL+Z followed by Enter in an empty line 

```
C:\Program Files\Microsoft Visual Studio\2022\Community> doskey yml2proc=py %YML_HOME%\yml2proc $*
C:\Program Files\Microsoft Visual Studio\2022\Community> yml2proc -My -
include yslt.yml2
tstylesheet template "/" | hello, world
^Z
hello, world
```

- clone pEpEngineSequoiaBackend and build it in debug mode using nmake; this will create `target\debug\pep_engine_sequoia_backend.dll`

```
C:\Program Files\Microsoft Visual Studio\2022\Community> cd %USERPROFILE%\source\repos
C:\Users\vb\source\repos> git clone ssh://newgitea@gitea.pep.foundation:23065/pEp.foundation/pEpEngineSequoiaBackend.git
C:\Users\vb\source\repos> cd pEpEngineSequoiaBackend
C:\Users\vb\source\repos\pEpEngineSequoiaBackend> nmake /f NMakefile /e DEBUG=debug

Microsoft (R) Program Maintenance Utility, Version 14.34.31937.0
Copyright (C) Microsoft Corporation. Alle Rechte vorbehalten.

        cargo build --features crypto-cng --no-default-features
    Finished dev [unoptimized + debuginfo] target(s) in 3.42s

Built target\debug\pep_engine_sequoia_backend.dll
```
- go back to the repos directory and clone asn1c; check out release 0.9.28-fdik of this fork:

```
C:\Users\vb\source\repos\pEpEngineSequoiaBackend> cd ..
C:\Users\vb\source\repos> git clone https://github.com/fdik-win/asn1c.git
C:\Users\vb\source\repos> cd ascn1c
C:\Users\vb\source\repos\asn1c> git switch mingw64
Updating files: 100% (1365/1365), done.
branch 'mingw64' set up to track 'origin/mingw64'.
Switched to a new branch 'mingw64'
```

- in the MSYS2 MSYS command line (the purple one, not in MSYS2 CLANG64, which is the orange one) go to the directory with the repo, install autotools and build asn1c using GCC (it will not build using CLANG)

```
$ cc --version
cc (GCC) 11.3.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


$ cd $(cygpath $USERPROFILE)/source/repos/asn1c
$ pacman -S autotools
$ autoreconv -iv
$ ./configure --prefix=$HOME
$ make && make install
$ ~/bin/asn1c -v
ASN.1 Compiler, v0.9.28
Copyright (c) 2003-2016 Lev Walkin <vlm@lionet.info>

```
- add the path to `asn1c.exe` to your user environment PATH variable; with this command you can copy the required path to the clipboard:

```
$ cygpath -w ~/bin | clip
```

- restart x64 Native Tools Command Prompt for VS 2022 and test if asn1c works:

```
C:\Program Files\Microsoft Visual Studio\2022\Community> asn1c -v
ASN.1 Compiler, v0.9.28
Copyright (c) 2003-2016 Lev Walkin <vlm@lionet.info>

```

Congratulations! When this works you have a working tool chain on your machine.
